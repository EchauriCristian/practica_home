<?php

    class Validation{

        protected $_errors;
        protected $_valid;

        public function __construct(){
            $this->_errors = array();
            $this->_valid = true;
        }

        public function getValid(){
            return $this->_valid;
        }

        public function isEmpty($field, $value){
            if(empty($value)){ 
                $this->_valid= false;
                array_push($this->_errors, array(
                    "message" => "El campo $field esta vacio.",
                    "id" => $field
                ));
            }
        }

        public function inLengthRange($field, $value, $minrange, $maxrange){
            if(strlen($value)<$minrange || strlen($value)>$maxrange){
                $this->_valid= false;
                array_push($this->_errors, array(
                    "message" => "El campo $field debe contener entre $minrange y $maxrange caracteres.",
                    "id" => $field
                ));
            }
        }
        
        public function formated($field, $value, $regexp){
            if(!preg_match($regexp, $value)){
                $this->_valid= false;
                array_push($this->_errors, array(
                    "message" => "El campo " . $field . " no cumple el formato requerido.",
                    "id" => $field
                ));
            }
        }


        public function isFileEmpty($field, $value){

            if($value < 1){
                $this->_valid= false;
                array_push($this->_errors, array(
                    "message" => "El archivo ". $field ." esta vacío.",
                    "id" => $field
                ));
            }
        }

        public function maxFileSize($field, $value, $maxSize){

            if ($value > $maxSize){
                $this->_valid=false;
                array_push($this->_errors, array(
                    "message" => "El archivo " . $field . " excede el peso máximo de 2 MegaBytes.",
                    "id" => $field
                ));
            }
        }

        public function acceptedFileFormats($field, $value, $formats){

            //IMPORTANTE HAY QUE DESCOMENTAR LA LINEA EXTENSION:PHP_FILEINFO.DLL EN EL ARCHIVO .INI
        
            //finfo_open carga un recurso, 'archivo', magic de la base de datos
            $finfo_mime = finfo_open(FILEINFO_MIME_TYPE);
            //finfo_file aplica el recurso cargado con finfo_open a un file
            $file_mime = finfo_file($finfo_mime, $value["tmp_name"]);

            if (!in_array($file_mime, $formats)){
                $this->_valid=false;
                array_push($this->_errors, array(
                    "message" => "El archivo " . $field . " no tiene un formato compatible.",
                    "id" => $field
                ));
            }
        }

        public function getValues(){
            return get_object_vars($this);
        }
    }
?>