$(document).ready(function () {
    //Esto quizas se vaya a la puta. No hay ningun momento en la web que me interese hacer esto.
    loadLibraries();
});

function loadLibraries(){

    if ($("#ckeditor").length) {
        CKEDITOR.replace("ckeditor");
    }

    if ($("select").length > 0) {
        $(".select2").select2({
            placeholder: "Elige una opcion",
            allowClear: true
        });
    }
}

//This will be .adminAction yep
//Control for Admin Panel
//This function listens for an admin action and filters
//each action to a corresponding method on 
//the php Requests
//IMPORTANTE: SI NO UTILIZAMOS $(DOCUMENT).ON Y TENEMOS MAS DE UN BOTON CON MISMA ID/CLASE, 
//DESPUES DE LA PRIMERA LLAMADA A AJAX, LAS DEMÁS NO SE DISPARARÁN.
//$(".userAction").click(function(event){});
$(document).on("click", ".userAction", function (event) {

    event.preventDefault();

    if ($("#ckeditor").length) {
        CKEDITOR.instances.ckeditor.updateElement();
    }

    //If admin wants to create or update a registry, we put the data from the html form into a FormData
    //and append the action key=>value (this may be done with a hidden input, should be cleaner)
    //Url for ajax to call is in html form's action attribute
    //-------
    //If the admin wants to delete or read a registry, we just get the action and the id of
    //the desired elemment and append them to a FormData.
    //Url for ajax to call is in button's formaction attribute, althought the button itself isn't inside a html form.
    //-------
    //There is also an ajaxIndex call by an index button, wich refresh or shows the current model list.
    if ($(this).attr("id") == "create" || $(this).attr("id") == "update") {

        var formId = "#" + $(".admin-form").attr("id");
        var url = $(formId).prop("action");
        var formdata = new FormData($(formId)[0]);
        formdata.append("action", $(this).attr("id"));

        ajaxCrud(url, formdata)
    }
    else if ($(this).attr("id") == "delete") {

        var url = $(this).attr("formaction");
        var formdata = new FormData();
        formdata.append("id", $(this).val());
        formdata.append("action", $(this).attr("id"));

        ajaxCrud(url, formdata)
    }
    else if ($(this).attr("id") == "index") {
        var url = $(this).attr("formaction");
        ajaxIndex(url);
    }
});


function ajaxCrud(url, formdata) {

    //Server call using ajax.
    //We pass url from form's formaction or button's action as url, formdata as data.
    //If conection to the server is succsessful, we filter the response by action.
    //Delete, Create and Update, on SQL success, call ajaxIndex (wich will show the user list) 
    //and gives it (1) the (model)-list.php url by grabbing it from the refresh/return button wich is 
    //on the view and (2) the ajax response we have just get from this ajax.
    //Create and update shows messages for validation and don't change view if input is wrong.
    $.ajax({
        type: "POST",
        url: url,
        data: formdata,
        processData: false,
        contentType: false,
        dataType: "json",
        cache: false,
        success: function (response) {

            if (response._action == "delete") {
                var url = $("#index").attr("formaction");
                ajaxIndex(url, response);
            }
            else if (response._action == "create" || response._action == "update") {

                if (response._valid) {
                    var url = $("#index").attr("formaction");
                    ajaxIndex(url, response);
                }
                else {
                    $("#errorWindow").html("");
                    $.each(response._errors, function (i, item) {
                        if (item.message) {
                            $("#errorWindow").html($("#errorWindow").html() + item.message + "<br/>");
                        }
                    });
                }
            }
        },
        error: function (event, jqxhr, settings, thrownError) {
            console.log("Ajax error event");
            console.log(event);
            console.log("Ajax error jqxhr");
            console.log(jqxhr);
            console.log("Ajax error settings");
            console.log(settings);
            console.log("Ajax error thrownError");
            console.log(thrownError);
        }
    });
}


//ajaxIndex reloads the view to show again the full list of registries.
//This function is called when a successful create, edit or delete is made.
//ALERT: we pass the previous ajax response to ajaxIndex as the parameter 'data'
//so we can properly show the create, edit or delete result message.
//url is the php Views/Component file where is defined the html list of registries.
function ajaxIndex(url, data) {
    $.ajax({
        url: url,
        success: function (response) {
            $(".view-container").html(response);

            if (typeof data != 'undefined') {
                $("#errorWindow").html(data._message);
            }
        }
    });
}


//If we want to modify a regisrty:
//We first check for all the DOM elemments with a given class, wich usually 
//will be a combination of "td-data" plus the id of a registry (1). 
//Then we will look at each elemment and asign a key=>value to an array 
//named 'data' where key is that elemment's name and value is said elemment's value.
//Then we just send 'data' to the server and load the view for that registry.

//(1 : "td-" as we are showing registries mostly as a html table. Not all registries or )
//(     all of its values will be contained in a html's table/td elemment respectively, )
//(     but said container will mostly work as it does a td in a table.                 )
$(document).on("click", ".viewLoader", function (event) {

    event.preventDefault();

    var url = $(this).attr('formaction');
    var objData = $(".td-" + $(this).attr("id"));
    var data = {};

    objData.each(function () {
        data[$(this).attr("name")] = $(this).html();
    });

    $.ajax({
        type: "POST",
        url: url,
        data: data,
        cache: false,
        success: function (response) {
            $(".view-container").html(response);
            loadLibraries();
        },
        error: function (response) {
            console.log(response);
        }
    });
});