<?php

    require_once($_SERVER['DOCUMENT_ROOT'] . "/practica-home/core/Database.php");
    
    class Blog extends Database {

        protected $_title;
        protected $_category;
        protected $_text;
        protected $_date;

        public function __construct(){

            parent::__construct();
            /*
            $this->_title = $_data["title"];
            $this->_category = $_data["category"];
            $this->_text = $_data["text"];
            $this->_date = date("r");
            */
        }

        //GETTES
        public function getTitle(){
            return $this->_tittle;
        }
        
        public function getCategory(){
            return $this->_category;
        }

        public function getText(){
            return $this->_text;
        }

        public function getDate(){
            return $this->_date;
        }

        public function getValues(){
            return get_object_vars($this);
        }

        //SETTES
        public function setTitle($title){
            $this->_title = $title;
        }

        public function setCategory($category){
            $this->_category = $category;
        }

        public function setText($text){
            $this->_text = $text;
        }

        public function setDate($date){
            $this->_date = $date;
        }


        //CRUD methods

        public function createBlog($blog){
            $query = "INSERT INTO t_blog (title, category, text) 
            VALUES (:title, :category, :text)";

            $stmt = $this->_pdo->prepare($query);
            $stmt->bindParam('title', $blog['title']);
            $stmt->bindParam('category', $blog['category']);
            $stmt->bindParam('text', $blog['text']);
            //$stmt->bindParam('date', $blog['date']);
            $stmt->execute();

            $blog_id = $this->_pdo->lastInsertId();

            return 'Blog registrado correctamente con el número de id ' . $blog_id;
        }

        public function showBlog($id){
            $query = "SELECT * 
            FROM `t_blog` 
            WHERE id = :id";

            $stmt = $this->_pdo->prepare($query);
            $stmt->bindParam('id', $id);
            $stmt->execute();
            $result = $stmt->fetch(PDO::FETCH_ASSOC);

            return $result;
        }

        public function updateBlog($blog){
            $query = "UPDATE `t_blog` 
            SET title = :title, category = :category, text = :text 
            WHERE id = :id";

            $stmt = $this->_pdo->prepare($query);
            $stmt->bindParam('title', $blog['title']);
            $stmt->bindParam('category', $blog['category']);
            $stmt->bindParam('text', $blog['text']);
            $stmt->bindParam('id', $blog['id']);
            $stmt->execute();

            return 'El blog con id ' . $blog["id"] . ' ha sido actualizado';
        }

        public function deleteBlog($id){
            $query = "DELETE 
            FROM `t_blog` 
            WHERE id = :id";

            $stmt = $this->_pdo->prepare($query);
            $stmt->bindParam('id', $id);
            $stmt->execute();

            return 'El blog con id ' . $id . ' ha sido eliminado.';
        }

        public function indexBlog(){
            $query = "SELECT * FROM t_blog";
            $stmt = $this->_pdo->prepare($query);
            $stmt->execute();
            $result = $stmt->fetchAll();
            return $result;
        }
    }
?>