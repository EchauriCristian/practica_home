<?php

    require_once($_SERVER['DOCUMENT_ROOT'] . "/practica-home/core/Database.php");

    class Slide extends Database {

        protected $_img_url;
        protected $_title;
        protected $_description;
        protected $_link;
        
        public function __construct(){
            parent::__construct();
        }

        //Aqui irian los gettes y settes

        public function createSlide($slide, $imgurl){
            $query = "insert into t_slide (img_url, title, description, link) 
            values (:img_url, :title, :description, :link)";

            $stmt = $this->_pdo->prepare($query);
            $stmt->bindParam("img_url", $imgurl);
            $stmt->bindParam("title", $slide["title"]);
            $stmt->bindParam("description", $slide["description"]);
            $stmt->bindParam("link", $slide["link"]);
            $stmt->execute();

            return "Slide guardado correctamente.";
        }

        public function showSlide($id){
            
        }
    
        public function updateSlide($id, $slide){
            
        }
    
        public function deleteSlide($id){

        }
    }
?>