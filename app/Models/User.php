<?php

    require_once($_SERVER['DOCUMENT_ROOT'] . "/practica-home/core/Database.php");

    class User extends Database {

        protected $_name;
        protected $_lastname;
        protected $_email;
        protected $_password;

        public function __construct(){
            parent::__construct();
        }

        //GETTES
        public function getName(){
            return $this->_name;
        }

        public function getLastname(){
            return $this->_lastname;
        }

        public function getEmail(){
            return $this->_email;
        }

        public function getPassword(){
            return $this->_password;
        }

        //SETTES
        public function setName($name){
            $this->_name = $name;
        }

        public function setLastname($lastname){
            $this->_lastname = $lastname;
        }

        public function setEmail($email){
            $this->_email = $email;
        }

        public function setPassword($password){
            $this->_password = $password;
        }

        /*
          Method that adds a new element to the User table.
          We first prepare the query statement,
          then we put that query into the pdo.
          With bindParam we link each var in $user
          with a table column.
          Then we execute the query.
          $stmt stands for statement.
        */
        public function createUser($user){
            
            $query = "INSERT INTO t_user (name, lastname, email, password) 
            VALUES (:name, :lastname, :email, :password)";

            $stmt = $this->_pdo->prepare($query);
            $stmt->bindParam("name", $user["name"]);
            $stmt->bindParam("lastname", $user["lastname"]);
            $stmt->bindParam("email", $user["email"]);
            $stmt->bindParam("password", $user["password"]);
            $stmt->execute();

            $user_id = $this->_pdo->lastInsertId();

            return 'Usuario añadido correctamente con el número de id ' . $user_id;
        }

        //Method for searching for a specific User by its id and return its data.
        //Introducimos los valores en el query mediante bindParam para 
        //ahorrarnos el trabajo de escribir el string con los simbolos especiales.
        public function showUser($id){

            $query = "SELECT *
            FROM `t_user`
            WHERE id = :id";

            $stmt = $this->_pdo->prepare($query);
            $stmt->bindParam("id", $id);
            $stmt->execute();
            $result = $stmt->fetch(PDO::FETCH_ASSOC);

            return $result;
        }


        public function updateUser($user){
            $query = "UPDATE `t_user` 
            SET name = :name, lastname = :lastname, email = :email 
            WHERE id = :id";

            $stmt = $this->_pdo->prepare($query);
            $stmt->bindParam("name", $user["name"]);
            $stmt->bindParam("lastname", $user["lastname"]);
            $stmt->bindParam("email", $user["email"]);
            $stmt->bindParam("id", $user["id"]);
            $stmt->execute();

            return 'El usuario con el id ' . $user["id"] . ' ha sido actualizado';
        }

        //Simply deletes a user searching it by ID
        public function deleteUser($id){
            $query = "DELETE
            FROM `t_user`
            WHERE id = :id";

            $stmt = $this->_pdo->prepare($query);
            $stmt->bindParam("id", $id);
            $stmt->execute();

            return 'El usuario con el id ' . $id . ' ha sido eliminado.';
        }


        //We use fetchAll to store all registries wich matched
        //the query in a var as an assosiative array.
        public function indexUser(){
            $query = "SELECT * FROM t_user";
            $stmt = $this->_pdo->prepare($query);
            $stmt->execute();
            $result = $stmt->fetchAll();
            return $result;
        }
    }
?>