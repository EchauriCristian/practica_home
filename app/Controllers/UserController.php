<?php

require_once($_SERVER['DOCUMENT_ROOT'] . "/practica-home/app/Models/User.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/practica-home/core/password.php");

Class UserController {

    protected $_user;

    //Llamamos al constructor de User
    public function __construct(){
        $this->_user = new User();
    }

    //Accedemos al conjunto CRUD
    public function createUser($user){
        $user["password"] = password_hash($user["password"],PASSWORD_DEFAULT);
        return $this->_user->createUser($user);
    }

    public function showUser($id){
        return $this->_user->showUser($id);
    }

    public function updateUser($user){
        return $this->_user->updateUser($user);
    }

    public function deleteUser($id){
        return $this->_user->deleteUser($id);
    }

    public function indexUser(){
        return $this->_user->indexUser();
    }
}
?>