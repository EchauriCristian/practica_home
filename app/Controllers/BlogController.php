<?php
    require_once($_SERVER['DOCUMENT_ROOT'] . '/practica-home/app/Models/Blog.php');

    Class BlogController {

        protected $_blog;

        public function __construct(){
            $this->_blog = new Blog();
        }

        public function createBlog($blog){
            return $this->_blog->createBlog($blog);
        }

        public function showBlog($id){
            return $this->_blog->showBlog($id);
        }

        public function updateBlog($blog){
            return $this->_blog->updateBlog($blog);
        }

        public function deleteBlog($id){
            return $this->_blog->deleteBlog($id);
        }

        public function indexBlog(){
            return $this->_blog->indexBlog();
        }
    }
?>