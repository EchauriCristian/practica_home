<?php

    require_once("../Models/Slide.php");

    Class SlideController{

        protected $_slide;

        public function __construct(){
            $this->_slide = new Slide();
        }

        //conjunto CRUD
        public function createSlide($slide, $imgurl){
            return $this->_slide->createSlide($slide, $imgurl);
        }

        public function showSlide($id){
            return $this->_slide->showSlide($id);
        }
    
        public function updateSlide($slide){
            return $this->_slide->updateSlide($slide);
        }
    
        public function deleteSlide($id){
            return $this->_slide->deleteSlide($id);
        }

    }
?>