<?php
    require_once($_SERVER['DOCUMENT_ROOT'] . '/practica-home/core/Validation.php');
    require_once($_SERVER['DOCUMENT_ROOT'] . '/practica-home/app/Controllers/BlogController.php');

    if (!empty($_POST) && isset($_POST['action'])){
    
        if ($_POST['action'] == 'create' || $_POST['action'] == 'update'){

            $validation =  new Validation();

            //title validation
            $validation->isEmpty("titulo",$_POST['title']);
            $validation->inLengthRange("titulo",$_POST['title'],2,30);
        
            //category validation
            $validation->isEmpty("categoria", $_POST['category']);
        
            //text validation
            $validation->isEmpty("texto", $_POST['text']);
            $validation->inLengthRange("texto",$_POST['text'],25,255);
        

            if($validation->getValid()){
                $blog = new BlogController();

                $response['_action'] = $_POST['action'];
                $response['_valid'] = $validation->getValid();

                if ($_POST['action'] == 'update' && !empty($blog->showBlog($_POST['id']))){
                    $response['_message'] = $blog->updateBlog($_POST);
                }
                else{
                    $response['_message'] = $blog->createBlog($_POST);
                }
            }
            else{
                $response = $validation->getValues();
                $response['_action'] = $_POST['action']; 
            }

            echo json_encode($response);
        }


        //ATENCION TESTEAR
        else if ($_POST['action'] == 'show'){

            $blog = new BlogController();

            $response = $blog->showBlog($_POST['id']);
            
            if (empty($response)){
                $response['_message'] = 'No se ha encontrado ningún blog con el id ' . $_POST['id'] . '. Se ha actualizado la lista.';
            }

            $response['_action'] = $_POST['action'];

            echo json_encode($response);
        }

        else if ($_POST['action'] == 'delete'){

            $blog = new BlogController();

            $response['_action'] = $_POST['action'];

            if (empty($blog->showBlog($_POST['id']))){
                $response['_message'] = 'No se ha encontrado ningún blog con el id ' . $_POST['id'] . '. Se ha actualizado la lista.';
            }
            else{
                $response['_message'] = $blog->deleteBlog($_POST['id']);
            }

            echo json_encode($response);
        }
    }
?>