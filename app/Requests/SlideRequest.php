<?php

    require_once($_SERVER['DOCUMENT_ROOT'] . "/practica-home/app/Controllers/SlideController.php");
    require_once($_SERVER['DOCUMENT_ROOT'] . "/practica-home/core/Validation.php");

    if (!empty($_POST) && isset($_POST['action'])){
        
        if ($_POST['action'] == 'create' || $_POST['action'] == 'update'){

            $validation = new Validation();

            //validacion de title, text y link


            //file validation
            $validation->isEmpty("imagen", $_FILES["image"]["name"]);

            if ($validation->getValid()){
                $validation->maxFileSize("imagen", $_FILES["image"]["size"], 2000000);
                $validation->acceptedFileFormats("imagen", $_FILES["image"], array("image/jpg","image/jpeg","image/png"));
                /*
                //para coger la extenson en el name del file
                $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
                //coger el width y length de la imagen
                getimagesize($_FILES["slide-img"]["tmp_name"]);
                */
            }

            if($validation->getValid()){
                //Definimos la carpeta donde se guardara el archivo y el nombre que tendrá dentro de esa carpeta
                $upload_dir = '../../uploads/img/' . basename($_FILES['image']['name']);

                //Movemos el archivo temporal a la carpeta fisica
                move_uploaded_file($_FILES["image"]["tmp_name"], $upload_dir);

                $slide = new SlideController();
                $response['_validation'] = $validation->getValid();
                $response['_message'] = $slide->createSlide($_POST, $upload_dir);
                echo json_encode($response);
            }
            else{
                echo json_encode($validation->getValues());
            }
        }
        else if ($_POST['action'] == 'delete'){

        }
    }
?>