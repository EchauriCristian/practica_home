<?php
    require_once($_SERVER['DOCUMENT_ROOT'] . '/practica-home/core/Validation.php');
    require_once($_SERVER['DOCUMENT_ROOT'] . '/practica-home/app/Controllers/UserController.php');
    
    if (!empty($_POST) && isset($_POST['action'])){

        
        if ($_POST['action'] == 'create'){

            $validation = commonValidations();

            //password validation
            $validation->isEmpty('contraseña',$_POST['password']);
            $validation->inLengthRange('contraseña', $_POST['password'], 8,8);
            

            if($validation->getValid()){
                $user = new UserController();
                $response['_action'] = $_POST['action'];
                $response['_valid'] = $validation->getValid();
                $response['_message'] = $user->createUser($_POST);
            }
            else{
                $response = $validation->getValues();
                $response['_action'] = $_POST['action']; 
            }

            echo json_encode($response);
        }
        //Delete control
        else if($_POST['action'] == 'delete'){

            $user = new UserController();
            $response['_action'] = $_POST['action'];

            if(!empty($user->showUser($_POST['id']))){
                $response['_message'] = $user->deleteUser($_POST['id']);
            }
            else{
                $response['_message'] = 'El usuario con id ' . $_POST['id'] . ' no ha sido encontrado. Se ha actualizado la lista.';
            }

            echo json_encode($response);
        }

        else if ($_POST['action'] == 'update'){

            $validation = commonValidations();

            if($validation->getValid()){
                $user = new UserController();

                $response['_action'] = $_POST['action'];

                if(empty($user->showUser($_POST['id']))){
                    $response['_valid'] = false;
                    $response['_message'] = 'El usuario con id ' . $_POST['id'] . ' no ha sido encontrado. Se ha actualizado la lista.';
                }
                else{
                    $response['_valid'] = $validation->getValid();
                    $response['_message'] = $user->updateUser($_POST);
                }
            }
            else{
                $response = $validation->getValues();
                $response['_action'] = $_POST['action']; 
            }
            
            echo json_encode($response);
        }
    }

    //Create and update common validations
    function commonValidations(){

        $validation = new Validation();

        $emailRegexp = '/^(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){255,})(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){65,}@)(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22))(?:\.(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-[a-z0-9]+)*\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-[a-z0-9]+)*)|(?:\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\]))$/iD';
        //could use W3c regexp too ^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$


        //Name validation
        $validation->isEmpty('nombre',$_POST['name']);
        $validation->inLengthRange('nombre', $_POST['name'], 2,20);
        $validation->formated('nombre',$_POST['name'],'/[A-Za-z]+/');

        //lastname validation
        $validation->isEmpty('apellido',$_POST['lastname']);
        $validation->inLengthRange('apellido', $_POST['lastname'], 2,20);
        $validation->formated('apellido',$_POST['lastname'],'/[A-Za-z]+/');

        //email validation
        $validation->isEmpty('email',$_POST['email']);
        $validation->inLengthRange('email', $_POST['email'], 5,40);
        $validation->formated('email',$_POST['email'], $emailRegexp);

        return $validation;
    }
?>