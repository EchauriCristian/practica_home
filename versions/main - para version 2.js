//Al cargar la página, miramos si hay mas de cero elementos en el html de la clase, id o etiqueta indicada, asi evitamos errorres y ejecutar codigo innecesariamente

$(document).ready(function() {

    if($("textarea").length>0){
       CKEDITOR.replace('comentary');
    }

    if($("select").length>0){
        $(".select2").select2();
     }

    /*Tambien podemos tener un if que cargue un ckeditor segun la configuracion
    
    if($("textarea").length>0){
       CKEDITOR.replaceAll(function(textarea, config) {
        if (textArea.className == "clase1"){

            config.removeButtons = "about";
            config.
        }
        else if (textArea.className == "clase2") {

            //otra configuracion
        }
       }
    }
    */
});


$(document).on("click", "#submit", function(event){

    CKEDITOR.instances.comentary.updateElement();

    event.preventDefault();

    var formData =  new FormData($("#contact-form")[0]);

    $.ajax({
        type : "POST",
        url : "verify-form.php",
        data : formData,
        processData: false,
        contentType: false,
        dataType: 'json',
        cache: false,
        success : function(response){
            if (response.success){
                $("#errorWindow").html("Yay");
            }
            else{
                $("#errorWindow").html("");
                $.each (response, function(key, value){
                    $("#errorWindow").html($("#errorWindow").html() + key + "<br/>");
                    $.each (value, function(subkey, subvalue){
                        $("#errorWindow").html($("#errorWindow").html()+ subkey +" "+ subvalue + "<br/>");
                    })
                });
            }
        },
        error: function(response){
            console.log(response);
        }
    });

    //  localhost/practica-home/contact-us.php


    /*
    //Metodos de jQuery Validation personalizados 

    jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-záéíóúàèìòùäëïöüñ\s]+$/i.test(value);
    }, "Sólo se aceptan letras");

    $.validator.addMethod("notDefault", function(value, element, arg){
            return this.optional(element) || value != "";
    }, "Value must not equal arg.");

    var form = $("#contact-form");

    form.validate({

        ignore: [],
        onfocusout: false,
        onkeyup: false,
        
        //errorClass: "class name",
        //validClass: "class name",
        //errorElement: "html element",
        
        rules: {
            name: {
                required: true,
                lettersonly: true,
                minlength: 2,
                maxlength: 20
            },

            email: {
                required: true,
                email: true,
                minlength: 5,
                maxlength: 40
            },

            phone:{
                required:true,
                digits: true,
                minlength: 9,
                maxlength: 9
            },

            "technology[]":{
                required:true,
                notDefault: true
            },

            comentary:{
                required: true,
                minlength: 50,
                maxlength:255
            }
        },

        messages: {
            name: {
                required: "Es obligatorio introducir un nombre.",
                lettersonly: "El nombre solo puede contener letras.",
                minlength: "El nombre debe contener al menos dos letras.",
                maxlength: "El nombre puede contener un maximo de 16 letras."
                
            },

            email: {
                required: "Introduce un email.",
                email: "Debes introducir un correo electronico válido, como ejemplo@dominio.com"
            },

            phone: {
                required: "Es obligatorio introducir un número de teléfono.",
                digits: "El número de teléfono solo admite dígitos.",
                minlength: "El número de teléfono debe constar con 9 dígitos.",
                maxlength: "El número de teléfono debe constar con 9 dígitos."
            },

            "technology[]":{
                required: "Seleccione una o más categorias para su comentario",
                notDefault: "Seleccione una o más categorias para su comentario"
            },

            comentary:{
                required: "Por favor, escriba su comentario."
            }
        }

        //showErrors
        //Aunque se supere la validacion de javascript, el servidor al validar, puede devolvernos error mediante showErrors, ej: el correo ya esta registrado
    });


    if (form.valid()){
        event.preventDefault();
        var formData =  new FormData($("#contact-form")[0]);
        
        $.mockjax({
            url : 'conect.html',
            isTimeout: false,
            responseText : "gracias."
        });

        $.ajax({
            type : "POST",
            url : "conect.html",
            data : formData,
            processData: false,
            contentType: false,
            success : function( data ){
                console.log( "First Method Data Saved: " , data );
                alert(data);
            },
            error: function(){
                console.log( "Error");
            }
        });

        
        var formData =  new FormData($("#contact-form")[0]);
        for(var pair of formData.entries()) {
            console.log(pair[0]+ ', '+ pair[1]);
        };
        
    }*/
});



$(document).on("click", "#rellenar", function(event){

});