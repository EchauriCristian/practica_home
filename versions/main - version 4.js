$(document).ready(function() {

    if($("textarea").length>0){
            CKEDITOR.replace("ckeditor");
    }

    if($("select").length>0){
        $(".select2").select2({
            placeholder: "Elige una opcion",
            allowClear: true
        });
    }
});

$(document).on("click","#submit",function(event){

    event.preventDefault();
    CKEDITOR.instances.ckeditor.updateElement();
    var formData = new FormData($("#submit").parent()[0]);
   
    $.ajax({
        type: "POST",
        url: $(this).attr("href"),
        data: formData,
        processData : false,
        contentType: false,
        dataType: "json",
        cache: false,
        success: function (response){

            if (response._valid == false){
                $("#errorWindow").html("");
                $.each (response._errors, function(i, item){
                    if(item.message){
                        $("#errorWindow").html($("#errorWindow").html()+ item.message + "<br/>");
                    }
                });
            }
            else{
                if (response._date){
                    $("#errorWindow").html("Blog guardado :" + response._date);
                }
                else{
                    $("#errorWindow").html("Bienvenido " + response);
                }
            }
        },
        error: function(response){
            console.log(response);
        }
    });
});


/*
//contact-form submit
$(document).on("click", "#contact-submit", function(event){

    CKEDITOR.instances.comentary.updateElement();
    event.preventDefault();
    var formData =  new FormData($("#contact-form")[0]);

    $.ajax({
        type : "POST",
        url : "verify-contact.php",
        data : formData,
        processData: false,
        contentType: false,
        dataType: 'json',
        cache: false,
        success : function(response){
            if (response._valid == false){
                $("#errorWindow").html("");
                //ACCEDEMOS AL ARRAY DENTRO DEL JSON CON EL '.' -> JSON.ARRAY -> response._errors
                //Con esto nos ahorramos el nestear bucles
                $.each (response._errors, function(i, item){
                    if(item.message){
                        $("#errorWindow").html($("#errorWindow").html()+ item.message + "<br/>");
                    }
                });
            }
            else{
                $("#errorWindow").html("Bienvenido " + response);
            }
        },
        error: function(response){
            console.log(response);
        }
    });
});
*/