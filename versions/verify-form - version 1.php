<?php
    
    if (empty($_POST['name']) || empty($_POST['email']) || empty($_POST['phone'])){
        echo json_encode(array(
            'success' => false,
            'message' => "Falta un dato" 
        ));
        return;
    }
    else if(strlen($_POST['name'])<2 || strlen($_POST['name'])>20 || strlen($_POST['email'])<5 || strlen($_POST['email']>40) || strlen($_POST['phone'])!=9){
        echo json_encode(array(
            'success' => false,
            'message' => "Error en la longitud" 
        ));
        return;
    }
    /*Se puede utilizar strpos(string texto, string aBuscar) === false*/
    else if(!preg_match('/[A-Za-z]+/', $_POST['name']) || !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) || !preg_match('/\d{9}/',$_POST['phone'])){
        echo json_encode(array(
            'success' => false,
            'message' => "Error de validacion" 
        ));
        return;
    }
    else{
        echo json_encode(array(
            'success' => true,
            'message' => "Ayyy LMAO" 
        ));
    }


    /*
    Con este bucle, podemos recorrer el POST entero y comprobar si alguna de sus kay esta vacia
    foreach ($_POST as $key => $value){
        if(empty($_POST[$key])){
            $valid = false;
        }
    }

    Llamamamos a las clases User y Comentary, creamos los objetos y devolvemos
    require_once "User.php";
    require_once "Comentary.php";

    $user = new User($_POST);
    $comentary = new Comentary($_POST);

    echo $user->getUserName();
    */

?>