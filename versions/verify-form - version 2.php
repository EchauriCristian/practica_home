<?php

    $response["success"]=true;
    
    //Name validation
    $response["name"] = [];
    if (empty($_POST['name'])){
        $response["success"]=false;
        $response["name"]["empty"] = "Introduzca el nombre.";
    }

    else{
        if(strlen($_POST['name'])<2 || strlen($_POST['name'])>20){
            $response["success"]=false;
            $response["name"]["length"] = "El nombre debe tener una longitud entre 2 y 20 caracteres.";
        }
        else{

            if(!preg_match('/[A-Za-z]+/', $_POST['name'])){
                $response["success"]=false;
                $response["name"]["format"]= "El nombre solo puede contener letras.";
            }
        }
    }

    //Email validation
    $response['email'] = [];
    if (empty($_POST['email'])){
        $response["success"]=false;
        $response['email']["empty"] = "Introduzca el email";
    }
    else{
        if(strlen($_POST['email'])<5 || strlen($_POST['email']>40)){
            $response["success"]=false;
            $response['email']["length"] = "El email debe tener una longitud entre 5 y 40 caracteres.";
        }
        else{
        //Se puede utilizar strpos(string texto, string aBuscar) === false
            if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
                $response["success"]=false;
                $response['email']["format"]= "Introduzca un email valido. Ej: micorreo@ejemplo.com";
            }
        }
    }

    //Phone validation
    $response['phone'] = [];
    if (empty($_POST['phone'])){
        $response["success"]=false;
        $response['phone']["empty"] = "Introduce el telefono";
    }
    else{
        if(!preg_match('/\d{9}/',$_POST['phone'])){
            $response["success"]=false;
            $response['phone']["format"]= "El telefono debe constar de 9 dígitos.";
        }
    }

    echo json_encode($response);

    /*
    Con este bucle, podemos recorrer el POST entero y comprobar si alguna de sus kay esta vacia
    foreach ($_POST as $key => $value){
        if(empty($_POST[$key])){
            $valid = false;
        }
    }

    Llamamamos a las clases User y Comentary, creamos los objetos y devolvemos
    require_once "User.php";
    require_once "Comentary.php";

    $user = new User($_POST);
    $comentary = new Comentary($_POST);

    echo $user->getUserName();
    */

?>