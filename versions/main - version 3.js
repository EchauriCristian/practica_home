$(document).ready(function() {

    if($("textarea").length>0){
       CKEDITOR.replace('comentary');
    }

    if($("select").length>0){
        $(".select2").select2();
    }
});


$(document).on("click", "#submit", function(event){

    CKEDITOR.instances.comentary.updateElement();

    event.preventDefault();

    var formData =  new FormData($("#contact-form")[0]);

    $.ajax({
        type : "POST",
        url : "verify-form.php",
        data : formData,
        processData: false,
        contentType: false,
        dataType: 'json',
        cache: false,
        success : function(response){
            if (response._valid == false){
                $("#errorWindow").html("");
                //ACCEDEMOS AL ARRAY DENTRO DEL JSON CON EL '.' -> JSON.ARRAY -> response._errors
                $.each (response._errors, function(i, item){
                    if(item.message){
                        $("#errorWindow").html($("#errorWindow").html()+ item.message + "<br/>");
                    }
                });
                /* El bucle de arriba "equivale" a este nesteo de bucles
                $.each (response, function(key, value){
                    $.each (value, function(subkey, subvalue){
                        if (subvalue.message){
                            $("#errorWindow").html($("#errorWindow").html()+ subvalue.message + "<br/>");
                        }
                    })
                });*/
            }
            else{
                $("#errorWindow").html("Bienvenido " + response);
            }
        },
        error: function(response){
            console.log(response);
        }
    });
});



$(document).on("click", "#rellenar", function(event){

});