<?php

    class Validation{

        protected $_response;

        public function __construct(){
            $this->_response["success"]=true;
        }

        public function getStatus(){
            return $this->_response["success"];
        }

        public function isEmpty($field, $value){
            if(empty($input)){ 
                $this->_response["success"]= false;
                array_push($this->_response, array(
                    "message" => "El campo $field esta vacio.",
                    "Id" => $field
                ));
                return true;
            }
            else{
                return false;
            }
        }

        public function inLengthRange($field, $value, $minrange, $maxrange){
            if(strlen($value)<$minrange || strlen($value)>$maxrange){
                $this->_response["success"]= false;
                array_push($this->_response, array(
                    "message" => "El campo $field debe contener entre $minrange y $maxrange caracteres.",
                    "id" => $field
                ));
                return false;
            }
            else{
                return true;
            }
        }

        //function fixed length
        //function predefined format?

        public function formated($field, $value, $regexp){
            if(!preg_match($regexp, $value)){
                $this->_response["success"]= false;
                array_push($this->_response, array(
                    "message" => "El campo $field no cumple el formato requerido.",
                    "id" => $field
                ));
                return false;
            }
            else{
                return true;
            }
        }

        public function getValues(){
            return get_object_vars($this);
        }
    }
?>