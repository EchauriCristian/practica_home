$(document).ready(function() {
    /*
    if($("textarea").length>0){
        CKEDITOR.replace("ckeditor");
    }
    */
    if($("select").length>0){
        $(".select2").select2({
            placeholder: "Elige una opcion",
            allowClear: true
        });
    }
});

$(document).on("click","#submit",function(event){

    event.preventDefault();
    /*
    if($("textarea").length>0){
        alert("entra");
        CKEDITOR.instances.ckeditor.updateElement();
    }
    */
    var form = "#" + $(".admin-form").attr("id");
    var url = $(form).prop("action");
    var formData = new FormData($(form)[0]);
    
    $.ajax({
        type: "POST",
        url: url,
        data: formData,
        processData : false,
        contentType: false,
        dataType: "json",
        cache: false,
        success: function (response){

            if (response._validation){
                $("#errorWindow").html(response._message);
            }
            else{
                $("#errorWindow").html("");
                $.each (response._errors, function(i, item){
                    if(item.message){
                        $("#errorWindow").html($("#errorWindow").html()+ item.message + "<br/>");
                    }
                });
            }
        },
        error: function(response){
            console.log(response);
        }
    });
});





//Control for user-admin
//Using JS Ajax we can operate on the data base
//but sadly, after the "refresh" with AjaxIndex
//no more ajax calls are executed.
//Pending of solution
$(".userAction").click(function(event){
    
    event.preventDefault();

    //var url = $(this).prop("formaction");
    var url = "app/Requests/UserRequest.php";
    var id = $(this).attr("id");
    var action = $(this).val();
    
    ajaxCall(url, id, action);
});


function ajaxCall(url, id, action){
    var xhttp =  new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200){
            ajaxIndex("Views/Components/users-list.php");
            $("#errorWindow").html("");
            $("#errorWindow").html(this.responseText);
        }
    }
    xhttp.open("POST", url, true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("id="+ encodeURIComponent(id) +"&action="+ encodeURIComponent(action));
}

function ajaxIndex(url){
    $(".view-container").html("");
    var xhttp =  new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200){
            $(".view-container").html(this.response);
        }
    }
    xhttp.open("POST", url, true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("action=none");
}
