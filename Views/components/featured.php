<!-- Services -->
			<div class="row col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 services">
				
				
				<h3>Our services</h3>

				<div class="offset-md-1 col-md-2 offset-lg-1 col-lg-2 offset-xl-1 col-xl-2 service">
					
					<div>
						<img src="public/img/articles/article1.png">
					</div>
					
					<div>
						<p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.</p>
					</div>
					
					<div>
						<a href="">
							<p>
								Leer mas+
							</p>
						</a>
					</div>

				</div>
				
				
				<div class="offset-md-2 col-md-2 offset-lg-2 col-lg-2 offset-xl-2 col-xl-2 service">
					
					<div>
						<img src="public/img/articles/article2.png">
					</div>
					
					<div>
						<p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.</p>
					</div>
					
					<div>
						<a href="">
							<p>
								Leer mas+
							</p>
						</a>
					</div>
				</div>
				
				
				<div class="offset-md-2 col-md-2 offset-lg-2 col-lg-2 offset-xl-2 col-xl-2 service">
					
					<div>
						<img src="public/img/articles/article3.png">
					</div>
					
					<div>
						<p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.</p>
					</div>
					
					<div>
						<a href="">
							<p>
								Leer mas+
							</p>
						</a>
					</div>
				</div>
			</div>