<?php
    require_once($_SERVER['DOCUMENT_ROOT'] . "/practica-home/app/Controllers/UserController.php");
?>

<div class="view-container">

    <div id="errorWindow"></div>
    <button  id="index" class="userAction" formaction="Views/components/users-list.php">Refresh</button></th>
    <TABLE>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Lastname</th>
            <th>Email</th>
            <th>Registry</th>
            <th><button  id="create" class="viewLoader" value="single/multiple" formaction="Views/components/register-form.php">Create</button></th>
        </tr>

        <?php 
            $user = new UserController();
            $list = $user->indexUser();

            foreach($list as $item) { ?>

                <tr class="tr<?= $item["id"]; ?>">
                    <td class="td-data<?=$item["id"];?>" name="id"> <?=$item["id"];?> </td>
                    <td class="td-data<?=$item["id"];?>" name="name"> <?=$item["name"];?> </td>
                    <td class="td-data<?=$item["id"];?>" name="lastname"> <?= $item["lastname"];?> </td>
                    <td class="td-data<?=$item["id"];?>" name="email"> <?=$item["email"];?> </td>
                    <td class="td-data<?=$item["id"];?>" name="create_at"> <?=$item["create_at"];?> </td>
                    <td> <button id="data<?= $item["id"]; ?>" class="viewLoader" value=<?=$item["id"];?> formaction="Views/components/register-form.php">Modify</button> </td>
                    <td> <button id="delete" class="userAction" value=<?=$item["id"];?> formaction="app/Requests/UserRequest.php">Delete</button> </td>
                </tr>
        <?php        
            }
        ?>
    </TABLE>

</div>