<?php
    require_once($_SERVER['DOCUMENT_ROOT'] . "/practica-home/app/Controllers/BlogController.php");
?>

<div class="view-container">

    <div id="errorWindow"></div>
    <button  id="index" class="userAction" formaction="Views/components/blog-list.php">Refresh</button></th>
    <TABLE>
        <tr>
            <th>ID</th>
            <th>Title</th>
            <th>Category</th>
            <th>Text</th>
            <th>Publication date</th>
            <th><button  id="create" class="viewLoader" value="single/multiple" formaction="Views/components/blog-form.php">Create</button></th>
        </tr>

        <?php 
            $blog = new BlogController();
            $list = $blog->indexBlog();

            foreach($list as $item) {  ?>
                
                <tr class="tr<?= $item["id"]; ?>">
                    <td class="td-data<?=$item["id"];?>" name="id"> <?=$item["id"];?> </td>
                    <td class="td-data<?=$item["id"];?>" name="title"> <?=$item["title"];?> </td>
                    <td class="td-data<?=$item["id"];?>" name="category"> <?= $item["category"];?> </td>
                    <td><textarea class="td-data<?=$item["id"];?>" name="text" rows="4" cols="100" readonly> <?= $item["text"]?> </textarea> </td>
                    <td class="td-data<?=$item["id"];?>" name="published_at"> <?=empty($item["published_at"])? "Not Published": $item["published_at"];?> </td>
                    <td> <button id="data<?= $item["id"]; ?>" class="viewLoader" value=<?=$item["id"];?> formaction="Views/components/blog-form.php">Modify</button> </td>
                    <td> <button id="delete" class="userAction" value=<?=$item["id"];?> formaction="app/Requests/BlogRequest.php">Delete</button> </td>
                </tr>
        <?php        
            }
        ?>
    </TABLE>

</div>