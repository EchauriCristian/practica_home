           <div class="row col-12 col-sm-12 offset-md-2 col-md-8 offset-lg-2 col-lg-8 offset-xl-2 col-xl-8">
		   		
		   <button  id="index" class="userAction" value="single/multiple" formaction="Views/components/blog-list.php">Return</button>

				<div  id="errorWindow" class="row col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12"></div>
				
				<form class="admin-form" id="blog-form" name="blog-form" action="app/Requests/BlogRequest.php">

					<div class="form-group">
						<label for="title">Title:</label><br/>
						<input type="text" id="title" name="title" value=<?= !empty($_POST) ? $_POST['title'] : '' ?>>
					</div>

					<div class="form-group">
						<label for="category">Category:</label><br/>
						<select class="blog-select select2" name="category">
							<!--Option vacio para el placeholder de select2 -->
                            <option></option>
                            <option value="innovation" <?= !empty($_POST) ? $_POST['category'] == 'innovation' ? "selected" : '' : '' ?>>Innovation</option>
                            <option value="programming" <?= !empty($_POST) ? $_POST['category'] == 'programming' ? "selected" : '' : '' ?>>Programming</option>
                            <option value="design" <?= !empty($_POST) ? $_POST['category'] == 'design' ? "selected" : '' : '' ?>>Web Design</option>
						</select>
					</div>

					<div class="form-group">
						<label for="ckeditor">Text:</label><br/>
						<textarea id="ckeditor" name="text"><?= !empty($_POST) ? $_POST['text'] : '' ?></textarea>
					</div>


					<?php
						if(!empty($_POST)){ ?>
							<input type="hidden" id="id" name="id" value=<?= !empty($_POST) ? $_POST["id"] : '' ?>>
							<button id="update" class="userAction">Update</button>
					<?php
						}
						else {
					?>
							<button id="create" class="userAction">Create</button>
					<?php
						}
					?>
				</form>
			</div>